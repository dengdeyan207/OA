from django.conf.urls import url


from yunying import views

urlpatterns = [
    url(r'^operating/$', views.OperatingView.as_view()),
    url(r'^del_operating/$', views.DelOperatingView.as_view()),
    url(r'^generaoperating/$', views.GeneraOperatTubeView.as_view()),
    url(r'^storeRefund/$', views.StoreRefund.as_view()),
    url(r'^refundInquiry/$', views.RefundInquiry.as_view()),
    # url(r'^updataRefund/$', views.UpdataRefund.as_view()),
    
    url(r'^update_generaoperating/$', views.UpdateGeneraOperatTubeView.as_view()),
    url(r'^some_collection/$', views.SomeCollectionView.as_view()),
    url(r'^update_some_collection/$', views.UpdateSomeCollectionView.as_view()),
    url(r'^some_situation/$', views.SomeSituationView.as_view()),
    url(r'^operatingSelf/$', views.OperatingSelf.as_view()),
    url(r'^deleteWithholding/$', views.DeleteWithholding.as_view()),
    url(r'^log/$', views.YunYingLogView.as_view()),
    url(r'^details_log/$', views.DetailsLogView.as_view()),
    url(r'^gendroperating/$', views.GeneraOperatingView.as_view()),
    url(r'^all_shop/$', views.AllShopView.as_view()),
    # url(r'^userLevel/$', views.UserLevel.as_view()),
     
]