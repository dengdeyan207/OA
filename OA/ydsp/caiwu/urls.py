from django.conf.urls import url


from caiwu import views

urlpatterns = [
    # url(r'^a/$', views.a.as_view()),
     url(r'^accountEntry/$', views.AccountEntry.as_view()),
     url(r'^accountInquire/$', views.AccountInquire.as_view()),
     url(r'^deleteAccount/$', views.DeleteAccount.as_view()),
     # url(r'^withholding/$', views.Withholding.as_view()),
     # url(r'^withholdingInquire/$', views.WithholdingInquire.as_view()),
     # url(r'^deleteWithholding/$', views.DeleteWithholding.as_view()),
     url(r'^baoxiao/$', views.SubmitExpenseView.as_view()),
     url(r'^baoxiao_type/$', views.SubmitTypeView.as_view()),
     url(r'^baoxiao_del/$', views.DelSubmitExpenseView.as_view()),
     # url(r'^expense/$', views.ExpenseView.as_view()),
     # url(r'^expense_del/$', views.DelExpenseView.as_view()),
     url(r'^financialpackage/$', views.FinancialPackageView.as_view()),
     url(r'^updateFinancial/$', views.UpdateFinancial.as_view()),
     # url(r'^attendanceData/$', views.AttendanceData.as_view()),
     url(r'^storeEntry/$', views.StoreEntry.as_view()),
     url(r'^updataStore/$', views.UpdataStore.as_view()),
     url(r'^getShop/$', views.GetShop.as_view()),
     url(r'^wage/$', views.WageView.as_view()),
     url(r'^del_deducted/$', views.Del_deducted.as_view()),
     url(r'^to_de_deducted/$', views.ToDeductedView.as_view()),
     url(r'^updataRefund/$', views.UpdataRefund.as_view()),
     url(r'^generaOperatTubeView/$', views.GeneraOperatTubeView.as_view()),
     # url(r'^financialStatements/$', views.FinancialStatements.as_view()),
     url(r'^income_type/$', views.IncomeTypeView.as_view()),
    url(r'^sum_income/$', views.SumIncomeView.as_view()),
    url(r'^send_sms/$', views.SendSMSView.as_view()),
    url(r'^user_wage/$', views.UserWageView.as_view()),


]