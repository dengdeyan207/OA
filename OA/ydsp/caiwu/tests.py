import datetime
import calendar
import time
import json
import os

def getYearDay(year):
    
    firstDayWeekDay, monthRange = calendar.monthrange(year, 12)

    # 获取当月的第一天
    firstDay = str(year) +'-01-01'
    lastDay = datetime.datetime(year=year, month=12, day=monthRange).strftime("%Y-%m-%d")

    return [firstDay, lastDay]

def timestr2stamp(timestr):
		""" 时间字符串转时间戳 """
		try:
			timeArray = time.strptime(timestr, "%Y-%m-%d")
		except:
			timeArray = time.strptime(timestr, "%Y-%m")
		timeStamp = int(time.mktime(timeArray))
		return timeStamp

def json2js_files(jsondata):
	js_str = json.dumps(jsondata,ensure_ascii = False).encode("utf-8")
	with open('test.json','wb') as f:
		f.write(js_str)


def get_1st_of_last_month():
    """
    获取上个月第一天的日期，然后加21天就是22号的日期
    :return: 返回日期
    """
    today=datetime.datetime.today()
    year=today.year
    month=today.month
    if month==1:
        month=12
        year-=1
    else:
        month-=1
    res=datetime.datetime(year,month,1).strftime("%Y-%m")
    return res


if __name__ == '__main__':

	print(get_1st_of_last_month())
	
	# print (getMonthFirstDayAndLastDay(2018,3))

# 	tss1 = {
#     "code": 200,
#     "result": "OK",
#     "data": {
#         "sy_details": {
#             "profit": {
#                 "m_profit": 1507,
#                 "y_profit": -89578
#             },
#             "input": {
#                 "m_income": 21670,
#                 "data": [
#                     {
#                         "style": "其它收入",
#                         "remarks": "",
#                         "amount": 10669
#                     },
#                     {
#                         "style": "软件",
#                         "remarks": "",
#                         "amount": 11001
#                     }
#                 ],
#                 "y_income": 21835
#             },
#             "output": {
#                 "m_output": 20163,
#                 "data": [
#                     {
#                         "style": "手续费",
#                         "remarks": "",
#                         "amount": 312
#                     },
#                     {
#                         "style": "打点",
#                         "remarks": "",
#                         "amount": 1000
#                     },
#                     {
#                         "style": "税费",
#                         "remarks": "",
#                         "amount": 10001
#                     },
#                     {
#                         "style": "职工薪酬",
#                         "remarks": "",
#                         "amount": 8750
#                     },
#                     {
#                         "style": "软件",
#                         "remarks": "",
#                         "amount": 100
#                     }
#                 ],
#                 "y_output": 111413
#             }
#         },
#         "sz_details": {
#             "sz_balance": 188266,
#             "currency_balance": 288266,
#             "qc_balance": 100000,
#             "input": {
#                 "inputsum_amount": 300000,
#                 "data": [
#                     {
#                         "style": "服务费",
#                         "remarks": "",
#                         "amount": 100000
#                     },
#                     {
#                         "style": "软件收入",
#                         "remarks": "",
#                         "amount": 200000
#                     }
#                 ]
#             },
#             "output": {
#                 "FinancialCost": [
#                     {
#                         "style": "手续费",
#                         "remarks": "",
#                         "amount": 312
#                     }
#                 ],
#                 "outputsum_amount": 111734,
#                 "ManagementCost": [
#                     {
#                         "style": "职工薪酬",
#                         "remarks": "",
#                         "amount": 100000
#                     },
#                     {
#                         "style": "软件",
#                         "remarks": "",
#                         "amount": 100
#                     }
#                 ],
#                 "OtherCost": [
#                     {
#                         "style": "服务费退款",
#                         "remarks": "",
#                         "amount": 321
#                     },
#                     {
#                         "style": "税费",
#                         "remarks": "",
#                         "amount": 10001
#                     },
#                     {
#                         "remarks": "",
#                         "sytle": "打点费",
#                         "amount": 1000
#                     }
#                 ]
#             }
#         }
#     }
# }
# 	print (json2js_files(tss1))


	# def timestr2stamp(timestr)
	# 	timeArray = time.strptime(timestr, "%Y-%m-%d")
	# 	timeStamp = int(time.mktime(timeArray))
	# 	return timeStamp

	# for i in [1,2,34,5]:
	# 	if i == 2:
	# 		continue
			
	# 	print (i)

