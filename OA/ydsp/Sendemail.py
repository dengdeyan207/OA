# -*- coding:utf-8 -*- 

import smtplib
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header
from email.utils import formataddr
from email.mime.application import MIMEApplication
import os


def Email(receiver, json):
    # 第二步发送文件
    sender = 'yd@yuetaosem.com'  # 发件人邮箱账号
    my_pass = 'JPwaxbm1314'  # 发件人授权码

    def mail():
        ret = True
        try:
            content =  "<html>\
                            <head>\
                            </head>\
                            <body>\
                                <p>店铺名："+json['shop_name']+"</p>\
                                <p>昨日销售额："+json['sales']+"</p>\
                                <p>昨日刷单金额："+json['brush_money']+"</p>\
                                <p>昨日访客数："+json['visitors']+"</p>\
                                <p>昨日转化率："+json['conversion_rate']+"</p>\
                                <p>昨日广告费："+json['advertising']+"</p>\
                                <p>昨日投入产出比："+json['input_rate']+"</p>\
                                <p>昨日广告费占比："+json['advertising_rate']+"</p>\
                                <p>昨日老顾客占比："+json['old_customer_rate']+"</p>\
                                <p>昨日加购率："+json['purchased']+"</p>\
                                <p>昨日收藏率："+json['collection_rate']+"</p>\
                                <p>昨日UV价值："+json['uv_value']+"</p>\
                                <p>昨日直通车销售额："+json['ztc_sales']+"</p>\
                                <p>昨日直通车消耗："+json['ztc_consumption']+"</p>\
                                <p>昨日直通车roi："+json['ztc_roi']+"</p>\
                                <p>昨日直通车加购量："+json['ztc_purchased']+"</p>\
                                <p>昨日直通车收藏量："+json['ztc_collection']+"</p>\
                                <p>昨日直通车加购成本："+json['ztc_purchase_cost']+"</p>\
                                <p>昨日直通车点击成本："+json['ztc_click_cost']+"</p>\
                                <p>昨日直通车UV价值："+json['ztc_uv_value']+"</p>\
                                <p>昨日钻展销售额："+json['zz_sales']+"</p>\
                                <p>昨日钻展消耗："+json['zz_consumption']+"</p>\
                                <p>昨日钻展roi："+json['zz_roi']+"</p>\
                                <p>昨日钻展加购量："+json['zz_purchased']+"</p>\
                                <p>昨日钻展收藏量："+json['zz_collection']+"</p>\
                                <p>昨日钻展加购成本："+json['zz_purchase_cost']+"</p>\
                                <p>昨日钻展点击成本："+json['zz_click_cost']+"</p>\
                                <p>昨日钻展UV价值："+json['zz_uv_value']+"</p>\
                                <p>今日事项："+json['content']+"</p>\
                            </body>\
                        </html>"
            msg = MIMEText(content,_subtype='html',_charset='gb2312')    #创建一个实例，这里设置为html格式邮件
            msg['From'] = formataddr(["有道尚品", sender])  # 括号里的对应发件人邮箱昵称、发件人邮箱账号
            msg['To'] = formataddr(["**", receiver])  # 括号里的对应收件人邮箱昵称、收件人邮箱账号
            msg['Subject'] = '店铺每日数据'  # 邮件的主题，也可以说是标题

            server = smtplib.SMTP_SSL("smtp.exmail.qq.com", 465)  # 发件人邮箱中的SMTP服务器，端口是25
            server.login(sender, my_pass)  # 括号中对应的是发件人邮箱账号、邮箱授权码
            server.sendmail(sender, receiver, msg.as_string())  # 括号中对应的是发件人邮箱账号、收件人邮箱账号、发送邮件
            server.quit()  # 关闭连接
        except Exception:  # 如果 try 中的语句没有执行，则会执行下面的 ret=False
            ret = False
        return ret

    return {'flag': mail()}


if __name__ == '__main__':
    receiver = '1774507011@qq.com'
    json = {
            "shop_name":'111111',
            "sales":'111111',
            "brush_money":'111111',
            "visitors":'111111',
            "conversion_rate":'112111',
            "advertising":'111111',
            "input_rate":'111111',
            "advertising_rate":'111111',
            "old_customer_rate":'111111',
            "purchased":'111111',
            "collection_rate":'111111',
            "uv_value":'111111',
            "ztc_sales":'111111',
            "ztc_consumption":'111111',
            "ztc_roi":'111111',
            "ztc_purchased":'111111',
            "ztc_collection":'111111',
            "ztc_purchase_cost":'111111',
            "ztc_click_cost":'111111',
            "ztc_uv_value":'111111',
            "zz_sales":'111111',
            "zz_consumption":'111111',
            "zz_roi":'111111',
            "zz_purchased":'111111',
            "zz_collection":'111111',
            "zz_purchase_cost":'111111',
            "zz_click_cost":'111111',
            "zz_uv_value":'111111',
            "content":'111111'
            }
    print(Email(receiver,json))
