from django.conf.urls import url


from xingzheng import views

urlpatterns = [
    url(r'^entry/$', views.EntryView.as_view()),
    url(r'^assets/$', views.AssetAllocaView.as_view()),
    url(r'^updateassets/$', views.UpdateAAssetView.as_view()),
    url(r'^attendanceData/$', views.AttendanceData.as_view()),
    url(r'^update_entry/$', views.UpdateEntryView.as_view()),
    url(r'^all_assets/$', views.AllAssetAllocaView.as_view()),
    url(r'^assets_name/$', views.AssetNameView.as_view()),
    url(r'^assets_serial/$', views.AssetSerialView.as_view()),
    url(r'^assets_user/$', views.AssetUserView.as_view()),
    url(r'^assets_ret/$', views.RetAssetView.as_view()),
]