from django.conf.urls import url


from oa import views

urlpatterns = [
    url(r'^users/$', views.UserView.as_view()),
    url(r'^userlogin/$', views.UserLongin.as_view()),
    url(r'^updatapwd/$', views.UpdataPwd.as_view()),
    url(r'^personalinformation/$', views.PersonalInformation.as_view()),
    url(r'^plan/$', views.WorkPlanView.as_view()),
    # url(r'^del_plan/(\d)/$', views.del_WorkPlanView.as_view()),
    url(r'^resume/$', views.ResumeView.as_view()),
    url(r'^adjust_job/$', views.AdjustJob.as_view()),
    url(r'^attendance/$', views.Attendance.as_view()),
    url(r'^updateAttendance/$', views.UpdateAttendance.as_view()),
    url(r'^getAttendance/$', views.GetAttendance.as_view()),
    url(r'^allpermissions/$', views.PermissionsView.as_view()),
    url(r'^assetRegistration/$', views.AssetRegistration.as_view()),
    url(r'^selectAssetReg/$', views.SelectAssetReg.as_view()),
    url(r'^createDepartment/$', views.CreateDepartment.as_view()),
    url(r'^acquisitionDepartment/$', views.AcquisitionDepartment.as_view()),
    url(r'^deleteAsset/$', views.DeleteAsset.as_view()),
    # url(r'^permissionMove/$', views.PermissionMove.as_view()),
    url(r'^open_perm/$', views.OpenPermissionsView.as_view()),
    url(r'^mesageQuery/$', views.MesageQuery.as_view()),
    url(r'^excellToJson/$', views.ExcellToJson.as_view()),
    url(r'^mesageStatus/$', views.MesageStatus.as_view()),
    url(r'^sel_work_plan/(\d)/$', views.SelWorkPlanView.as_view()),
    url(r'^work_details/$', views.WorkDetailsView.as_view()),


]