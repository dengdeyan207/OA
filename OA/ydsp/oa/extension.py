"""数据存储文件"""
import json
import logging

from database.connection import Link_database
# from ydsp.database.connection import Link_database

logger = logging.getLogger(__name__)


class WorkPlanSQL(object):
    """工作计划录入"""
    def __init__(self, statdate, is_type, department, title, content, user_id):
        self.statdate =statdate
        self.user_id = user_id
        self.is_type = is_type
        self.department = department
        self.title = title
        self.content =content
        self.con, self.obj = Link_database()

    def month_log(self):
        """月计划"""
        sql = """insert into month_log (statdate,is_type,department,title,content,user_id) values (%d,'%s','%s','%s','%s',%d)""" % (self.statdate,self.is_type, self.department, self.title, self.content, self.user_id)

        return sql

    # def weeks_log(self):
    #     """周计划"""
    #     sql = """insert into weeks_log (statdate,department,title,content,user_id) values (%d,'%s','%s','%s',%d)""" % (self.statdate, self.department, self.title, self.content, self.user_id)
    #
    #     return sql

    # def job_log(self):
    #     """日计划"""
    #     sql = """insert into job_log (statdate,name,title,file_address,user_id, shop_name) values (%d,'%s','%s','%s',%d,'%s')""" % (
    #     self.statdate, self.name, self.title, self.file_address, self.user_id, self.shop_name)
    #
    #     return sql

    def sel_email(self, shop_name):
        sql = """select email from genera_operat_tube where store_name='%s'""" % shop_name

        self.obj.execute(sql)
        q = self.obj.fetchone()
        if q:
            return q[0]

    def message(self):
        """消息推送"""
        sql = """select tube from depar_user where user_id=%d""" % self.user_id
        self.obj.execute(sql)
        z_id = self.obj.fetchone()[0]  # 主管id
        if not z_id:
            sql_1 = """select director from depar_user where user_id=%d""" % self.user_id
            self.obj.execute(sql_1)
            z_id = self.obj.fetchone()[0]  # 总监id
            if not z_id:
                z_id = 2  # 没有主管，总监直接推送到杨总

        sql_5 = """select id from month_log order by id desc limit 1"""
        self.obj.execute(sql_5)
        file_id = self.obj.fetchone()[0]

        sql_2 = """insert into message_table (file_id,send_id,receive_id,files_name,statdate) values (%d,%d,%d,'%s',%d)""" % (file_id, self.user_id, z_id, self.title, self.statdate)
        self.obj.execute(sql_2)

        sql_3 = """select id from depar_user where tube=%d""" % self.user_id
        self.obj.execute(sql_3)
        is_y = self.obj.fetchone()
        if is_y:  # 主管 工作计划 老板需要看到
            sql_4 = """insert into message_table (file_id, send_id,receive_id,files_name,statdate) values (%d,%d,%d,'%s',%d)""" % (
            file_id, self.user_id, 1, self.title, self.statdate)
            self.obj.execute(sql_4)

    def main(self):
        sql = self.month_log()
        self.obj.execute(sql)
        self.message()
        self.con.commit()

    def __del__(self):
        self.obj.close()
        self.con.close()


class del_WorkPlanSQL(object):
    """删除工作计划"""
    def __init__(self, id):
        self.id = id
        self.con, self.obj = Link_database()

    def month_log(self):
        sql = """delete from month_log where id=%d""" % self.id

        return sql

    def weeks_log(self):
        sql = """delete from weeks_log where id=%d""" % self.id

        return sql

    def job_log(self):
        sql = """delete from job_log where id=%d""" % self.id

        return sql

    def main(self, sql):
        self.obj.execute(sql)
        self.con.commit()

    def __del__(self):
        self.obj.close()
        self.con.close()


class ResumeSQL(object):
    """录入简历"""
    def __init__(self):
        self.con, self.obj = Link_database()

    def select_user(self, name):
        sql = """select id from user where name='%s'""" % name
        self.obj.execute(sql)
        q = self.obj.fetchone()
        if q:
            return q[0]

    def entry(self, apply_position, expected_salary, work_experience, education_experience, opinion, user_id):
        sql = """insert into resume (apply_position,expected_salary,work_experience,education_experience,opinion,user_id) values ('%s','%s','%s','%s','%s',%d)""" % (apply_position, expected_salary, work_experience, education_experience, opinion, user_id)
        self.obj.execute(sql)
        self.con.commit()

    def __del__(self):
        self.obj.close()
        self.con.close()


# def TotalAssetsSQL():
#     """总资产录入"""


class OpenPermissionsSQL(object):
    """权限开通"""
    def __init__(self, user_id, checked, sideNavData):
        self.user_id = user_id
        self.checked = checked
        self.sideNavData = sideNavData
        self.con, self.obj = Link_database()

    # def sel_data(self, category):
    #     """
    #     查询
    #     :return list
    #     """
    #     sql = """select %s from permission_table""" % category
    #     self.obj.execute(sql)
    #     q_str = self.obj.fetchone()[0]
    #     q_list = eval(q_str)
    #
    #     return q_list
    #
    # def inset_data(self, category, d_srt):
    #     """存储"""
    #     sql = """update permission_table set %s='%s'""" % (category, d_srt)
    #     self.obj.execute(sql)
    #     self.con.commit()
    #
    # def deal_with(self, q_list):
    #     """
    #     数据处理
    #     :return str
    #     """
    #     q_list.append(self.user_id)
    #     a = set(q_list)
    #     c = list(a)
    #     d_str = str(c)
    #
    #     return d_str

    def update_permissions(self):
        """更新权限"""
        # sql = """update permission_table set index_s=%d,finance=%d,operate=%d,self=%d,work=%d,log=%d,home=%d,edit=%d where user_id=%d""" % \
        #       (d_list[0], d_list[1], d_list[2], d_list[3], d_list[4], d_list[5], d_list[6], d_list[7], self.user_id)
        sql = """update permission_table set checked='{}',permission_data='{}' where user_id={}""".format(self.checked, self.sideNavData, self.user_id)
        self.obj.execute(sql)
        self.con.commit()

    # def deal_with(self):
    #     """数据处理"""
    #     d_list = [self.category['index_s'], self.category['finance'], self.category['operate'], self.category['self'],
    #      self.category['work'], self.category['log'], self.category['home'], self.category['edit']]
    #
    #     return  d_list

    def main(self):
        # for category in self.category[:-1].split('='):
        #     q_list = self.sel_data(category)
        #     d_str = self.deal_with(q_list)
        #     self.inset_data(category, d_str)
        # d_list = self.deal_with()
        # self.update_permissions(d_list)

        self.update_permissions()

    def __del__(self):
        self.obj.close()
        self.con.close()


def WorkDetailsSQL(id, notation, critics):
    """工作计划领导意见"""
    con, obj = Link_database()

    sql_1 = """select name from user where id=%d""" % critics
    obj.execute(sql_1)
    n = obj.fetchone()[0]

    a = {"key": n, "value": notation}

    sql_2 = """select notation from month_log where id=%d""" %id
    obj.execute(sql_2)
    data_str = obj.fetchone()[0]
    data = []
    if data_str:
        data = json.loads(data_str)
    data.append(a)

    data_str = json.dumps(data, ensure_ascii=False)

    sql = """update month_log set notation='%s' where id=%d""" % (data_str, id)
    obj.execute(sql)
    con.commit()

    obj.close()
    con.close()